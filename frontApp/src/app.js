import * as $ from "jquery";
import "bootstrap";
import "./scss/app.scss";

import "leaflet/dist/leaflet.css"

import Vue from "vue";
import App from "./App.vue"

$(() => {
    new Vue({
        el: "#app",
        render: h => h(App)
    });
});
