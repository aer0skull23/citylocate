import * as L from "leaflet";
import mIcon from "leaflet/dist/images/marker-icon.png";
import mShadow from "leaflet/dist/images/marker-shadow.png";

import "leaflet/dist/leaflet.css"

export default class Map {
    constructor() {
        this.map = L.map('map').setView([51.505, -0.09], 13);

        map.on('click', (e) => {
            console.log(e);
        });

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        L.marker([51.5, -0.09], {
            icon: L.icon({
                iconSize: [ 25, 41 ],
                iconAnchor: [ 13, 41 ],
                iconUrl: mIcon,
                shadowUrl: mShadow
            })
        }).addTo(map);
    }
}

