## Steps

1. `yarn install`
2. `yarn run webpack`
3. open index.html

The app will connect to http://localhost:8000, if your server API is somewhere else, you can change origin for all requests by editing `origin` field in `src/js/shared.js`
