<?php

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\Location;
use App\Repository\LocationRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LocationFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        // $product = new Product();
        // $manager->persist($product);

        $tab = [
            ["id" => 1, "ville" => "Belfort", "pays" => "France", "libelle" => "Lion de Bartholdi", "longitude" => 6.864565, "latitude" => 47.636683, "src" => "BelfortLion.jpg"],
            ["id" => 2, "ville" => "Belfort", "pays" => "France", "libelle" => "Rue pietonne", "longitude" => 6.856764, "latitude" => 47.637057, "src" => "BelfortRuePietonne.jpg"],
            ["id" => 3, "ville" => "Belfort", "pays" => "France", "libelle" => "Gare", "longitude" => 6.853725, "latitude" => 47.634433, "src" => "BelfortGare.jpg"],
            ["id" => 4, "ville" => "Belfort", "pays" => "France", "libelle" => "IUT", "longitude" => 6.840667, "latitude" => 47.643785, "src" => "BelfortIUT.jpg"],
            ["id" => 5, "ville" => "Belfort", "pays" => "France", "libelle" => "UTBM", "longitude" => 6.843972, "latitude" => 47.641436, "src" => "BelfortUTBM.jpg"],
            ["id" => 6, "ville" => "Belfort", "pays" => "France", "libelle" => "ESTA", "longitude" => 6.860807, "latitude" => 47.640380, "src" => "BelfortEsta.jpg"],
            ["id" => 7, "ville" => "Belfort", "pays" => "France", "libelle" => "One shot", "longitude" => 6.854804, "latitude" => 47.632852, "src" => "BelfortOneShot.jpg"],
            ["id" => 8, "ville" => "Belfort", "pays" => "France", "libelle" => "Mairie", "longitude" => 6.862874, "latitude" => 47.637931, "src" => "BelfortMairie.JPG"],
            ["id" => 9, "ville" => "Belfort", "pays" => "France", "libelle" => "Hopital", "longitude" => 6.853479, "latitude" => 47.644550, "src" => "BelfortHopital.jpg"],
            ["id" => 10, "ville" => "Essert (Grand Belfort)", "pays" => "France", "libelle" => "Patinoire", "longitude" => 6.831377, "latitude" => 47.632717, "src" => "BelfortPatinoire.JPG"]
        ];

        $city = new City();
        $city
            ->setName("Belfort")
            ->setLat(47.641858)
            ->setLng(6.851684)
            ->setZoom(13);

        $manager->persist($city);

        foreach ($tab as $l) {
            $location = new Location();
            $location
                ->setUrl($l["src"])
                ->setLat($l["latitude"])
                ->setLng($l["longitude"])
                ->setCity($city);

            $manager->persist($location);
        }
        $manager->flush();
    }
}
