<?php

namespace App\Controller;

use App\Entity\Party;
use App\Entity\User;
use App\Repository\CityRepository;
use App\Repository\PartyRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class APIController extends AbstractController
{
    /**
     * @Route("/getToken", name="getToken", methods={"GET"})
     * @param ObjectManager $manager
     * @param SessionInterface $session
     * @return Response
     */
    public function getToken(ObjectManager $manager, SessionInterface $session) {
        $session->start();
        $token = $session->getid();

        $user = new User();
        $user->setToken($token);

        $manager->persist($user);
        $manager->flush();

        $tok = $this->json(["token" => $token]);

        $response = new Response();

        $response->setContent(json_encode(["token" => $token]));
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/play/games", name="createParty", methods={"POST"})
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserRepository $userRepo
     * @param CityRepository $cityRepo
     * @return Response
     */
    public function createParty(Request $request, ObjectManager $manager, UserRepository $userRepo, CityRepository $cityRepo) {
        $name = $request->request->get("city");
        $userToken = $request->request->get("userToken");

        $response = new Response();
        $response->headers->set('Access-Control-Allow-Origin', '*');

        //TODO add constraint for unique name
        $city = $cityRepo->findOneBy(["name" => $name]);
        if ($city == null) {
            $response->setContent(json_encode(["error" => "city", "message" => "invalid city name"]));
            return $response;
        }

        //TODO same
        $user = $userRepo->findOneBy(["token" => $userToken]);
        if ($user == null) {
            $response->setContent(json_encode(["error" => "user", "message" => "unknown user"]));
            return $response;
        }

        $party = new Party();

        $party
            ->setCity($city)
            ->setToken(base64_encode(openssl_random_pseudo_bytes(50)))
            ->addUser($user);

        $manager->persist($party);
        $manager->flush();

        $response->setContent(json_encode($party->toJsonArray()));
        return $response;
    }

    /**
     * @Route("/login", name="login")
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserRepository $userRepo
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function login(Request $request, ObjectManager $manager, UserRepository $userRepo) {
        $token = $request->request->get("token");
        $username = $request->request->get("user");
        $pass = $request->request->get("pass");

        $response = new Response();
        $response->headers->set('Access-Control-Allow-Origin', '*');

        $user = $userRepo->findOneBy(["token" => $token]);

        if ($user == null) {
            $response->setContent(json_encode(["error" => "token", "message" => "not a valid token"]));
            return $response;
        }

        if ($user->getUsername() != null) {
            $response->setContent(json_encode(["error" => "user", "message" => "already connected"]));
            return $response;
        }

        $userLogged = $userRepo->findOneBy(["username" => $username, "password" => $pass]);

        if ($userLogged == null) {
            $response->setContent(json_encode(["error" => "user", "message" => "already connected"]));
            return $response;
        }

        $manager->remove($user);
        $userLogged->setToken($token);
        $manager->flush();

        $response->setContent(json_encode(["token" => $token, "username" => $username]));
        return $response;
    }
}
