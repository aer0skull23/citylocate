<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\JsonType;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PartyRepository")
 */
class Party {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City", inversedBy="parties")
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="party")
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    public function __construct() {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getCity(): ?City {
        return $this->city;
    }

    public function setCity(?City $city): self {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection {
        return $this->users;
    }

    public function addUser(User $user): self {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setParty($this);
        }

        return $this;
    }

    public function removeUser(User $user): self {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getParty() === $this) {
                $user->setParty(null);
            }
        }

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function toJsonArray() {
        $city = $this->getCity();
        $locations = [];
        foreach ($city->getLocations() as $location) {
            $locations[] = [
                "lat" => $location->getLat(),
                "lng" => $location->getLng(),
                "url" => $location->getUrl()
            ];
        }

        return [
            "partyToken" => base64_encode($this->getToken()),
            "cityName" => $city->getName(),
            "defaultLat" => $city->getLat(),
            "defaultLng" => $city->getLng(),
            "defaultZoom" => $city->getZoom(),
            "locations" => $locations
        ];
    }
}
