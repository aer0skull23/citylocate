<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190331221140 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_8D93D649213C1059');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, party_id, token, username, password FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, party_id INTEGER DEFAULT NULL, username VARCHAR(255) DEFAULT NULL COLLATE BINARY, token VARCHAR(255) DEFAULT NULL, password BLOB DEFAULT NULL, CONSTRAINT FK_8D93D649213C1059 FOREIGN KEY (party_id) REFERENCES party (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO user (id, party_id, token, username, password) SELECT id, party_id, token, username, password FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE INDEX IDX_8D93D649213C1059 ON user (party_id)');
        $this->addSql('DROP INDEX IDX_89954EE08BAC62AF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__party AS SELECT id, city_id, token FROM party');
        $this->addSql('DROP TABLE party');
        $this->addSql('CREATE TABLE party (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, city_id INTEGER DEFAULT NULL, token VARCHAR(255) DEFAULT NULL, CONSTRAINT FK_89954EE08BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO party (id, city_id, token) SELECT id, city_id, token FROM __temp__party');
        $this->addSql('DROP TABLE __temp__party');
        $this->addSql('CREATE INDEX IDX_89954EE08BAC62AF ON party (city_id)');
        $this->addSql('DROP INDEX IDX_5E9E89CB8BAC62AF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__location AS SELECT id, city_id, url, lat, lng FROM location');
        $this->addSql('DROP TABLE location');
        $this->addSql('CREATE TABLE location (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, city_id INTEGER DEFAULT NULL, url VARCHAR(255) NOT NULL COLLATE BINARY, lat DOUBLE PRECISION DEFAULT NULL, lng DOUBLE PRECISION DEFAULT NULL, CONSTRAINT FK_5E9E89CB8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO location (id, city_id, url, lat, lng) SELECT id, city_id, url, lat, lng FROM __temp__location');
        $this->addSql('DROP TABLE __temp__location');
        $this->addSql('CREATE INDEX IDX_5E9E89CB8BAC62AF ON location (city_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_5E9E89CB8BAC62AF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__location AS SELECT id, city_id, url, lat, lng FROM location');
        $this->addSql('DROP TABLE location');
        $this->addSql('CREATE TABLE location (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, city_id INTEGER DEFAULT NULL, url VARCHAR(255) NOT NULL, lat DOUBLE PRECISION DEFAULT NULL, lng DOUBLE PRECISION DEFAULT NULL)');
        $this->addSql('INSERT INTO location (id, city_id, url, lat, lng) SELECT id, city_id, url, lat, lng FROM __temp__location');
        $this->addSql('DROP TABLE __temp__location');
        $this->addSql('CREATE INDEX IDX_5E9E89CB8BAC62AF ON location (city_id)');
        $this->addSql('DROP INDEX IDX_89954EE08BAC62AF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__party AS SELECT id, city_id, token FROM party');
        $this->addSql('DROP TABLE party');
        $this->addSql('CREATE TABLE party (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, city_id INTEGER DEFAULT NULL, token BLOB DEFAULT NULL)');
        $this->addSql('INSERT INTO party (id, city_id, token) SELECT id, city_id, token FROM __temp__party');
        $this->addSql('DROP TABLE __temp__party');
        $this->addSql('CREATE INDEX IDX_89954EE08BAC62AF ON party (city_id)');
        $this->addSql('DROP INDEX IDX_8D93D649213C1059');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, party_id, token, username, password FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, party_id INTEGER DEFAULT NULL, username VARCHAR(255) DEFAULT NULL, token BLOB DEFAULT NULL, password BLOB DEFAULT NULL)');
        $this->addSql('INSERT INTO user (id, party_id, token, username, password) SELECT id, party_id, token, username, password FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE INDEX IDX_8D93D649213C1059 ON user (party_id)');
    }
}
