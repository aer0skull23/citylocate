## Setup

1. `composer install`
2. `php bin/console doctrine:database:create` with sqlite by default (see .env)
3. `php bin/console doctrine:migrations:migrate` applies sqlite migrations, if you uses others sgbd, you have to delete all migrations in `src/Migrations/` and make your own with `php bin/console make:migrations` before migrate
4. `php bin/console fixtures:load` to load default fixtures (with only Belfort as a city and 10 locations)
5. `yarn install` to install yarn packages
6. `yarn encore production` to compile assets
7. `php bin/console server:run` run API